#include "Enemy.h"
#include "Engine/Model.h"
#include "Engine/SphereCollider.h"

//コンストラクタ
Enemy::Enemy(GameObject* parent)
    :GameObject(parent, "Enemy"), hModel_(-1)
{
}

//デストラクタ
Enemy::~Enemy()
{
}

//初期化
void Enemy::Initialize()
{
    //モデルデータのロード
    hModel_ = Model::Load("Enemy.fbx");
    assert(hModel_ >= 0);

    transform_.position_.vecX = (float)(rand() % 6001 - 3000) / 100.0f; //int型〜float型に変えることをキャストという。
    transform_.position_.vecZ = 20.0f;

    SphereCollider* collision = new SphereCollider(XMVectorSet(0, 0, 0, 0), 1.2f);
    AddCollider(collision);
}

//更新
void Enemy::Update()
{
}

//描画
void Enemy::Draw()
{
    Model::SetTransform(hModel_, transform_);
    Model::Draw(hModel_);
}

//開放
void Enemy::Release()
{
}

//何かに当たった
void Enemy::OnCollision(GameObject* pTarget)
{
    //当たったときの処理
    if (pTarget->GetObjectName() == "Enemy") //情報が欲しいとき「Get〜」
    {
        KillMe();  //敵が消える
        pTarget->KillMe();  //
    }
}

